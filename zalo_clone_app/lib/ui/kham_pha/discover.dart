import 'package:flutter/material.dart';

class DiscoverScreen extends StatefulWidget {
  static const route = '/discover';
  const DiscoverScreen({Key? key}) : super(key: key);

  @override
  _DiscoverScreenState createState() => _DiscoverScreenState();
}

class _DiscoverScreenState extends State<DiscoverScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[300],
      body: SingleChildScrollView(
        child: Column(children: const [
          TopList(),
          BodyView()
        ],),
      ),
    );
  }
}


class TopList extends StatelessWidget {
  const TopList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List imgList = [
      'assets/tim_quanh_day.png',
      'assets/bag.png',
      'assets/sticker.png',
      'assets/goverment.png',
      'assets/coin.png',
      'assets/zalopay.png',
      'assets/phone.png',
      'assets/paybill.png',
    ];

    List itemsName = [
      'Tìm Quanh Đây',
      'Shop',
      'Sticker',
      'eGovernment',
      'Fiza',
      'Ví ZaloPay',
      'Nạp tiền ĐT',
      'Trả Hóa Đơn',
    ];
    return Container(
      decoration: BoxDecoration(color: Colors.white),
      padding: EdgeInsets.symmetric(vertical: 12),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 50,
            child: Text(
              'Tiện Ích',
              style: const TextStyle(
                fontSize: 20,
              ),),
          ),
          Container(
            height: 200,
            alignment: Alignment.center,
            child: GridView.builder(
              scrollDirection: Axis.horizontal,
              shrinkWrap: true,
              itemCount: imgList.length,
              gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                  maxCrossAxisExtent: 180,
                  childAspectRatio: 1.2,
                  crossAxisSpacing: 12,
                  mainAxisSpacing: 20),
              itemBuilder: (BuildContext context, int index) => Container(
                margin: EdgeInsets.only(left: 0),
                child: GestureDetector(
                  onTap: () {print(itemsName[index]);},
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image.asset(
                        imgList[index],
                        width: 45,
                        height: 45,
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 8),
                        child: Text(
                          itemsName[index],
                          textAlign: TextAlign.center,
                          style: const TextStyle(
                            fontSize: 13,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class BodyView extends StatelessWidget {
  const BodyView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String bodyTitle = 'Gợi ý Official Account';

    List imgList = [
      'assets/thong_tinh_chinh_phu.jpg',
      'assets/zalo_i.jpg',
      'assets/so_xo.jpg',
      'assets/dien_luc.jpg',
      'assets/bo_y_te.jpg',
      'assets/game.jpg',
    ];

    List itemsName = [
      'Thông tin chính phủ',
      'Zalo Insights',
      'Kết Quả Xổ Số MIỄN PHÍ',
      'TCT Điện lực Miền Nam',
      'Bộ Y Tế',
      'Cộng đồng Võ Lâm Truyền Kỳ'
    ];

    return Container(
      margin: EdgeInsets.symmetric(vertical: 12),
      decoration: BoxDecoration(color: Colors.white),
      padding: EdgeInsets.symmetric(vertical: 12),
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.only(left: 12, bottom: 12, top: 12),
                child: Text(
                  bodyTitle,
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ],
          ),
          Container(
            alignment: Alignment.center,
            height: 200,
            child: GridView.builder(
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              itemCount: imgList.length,
              gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                  maxCrossAxisExtent: 200,
                  childAspectRatio: 1.4,
                  crossAxisSpacing: 14,
                  mainAxisSpacing: 8),
              itemBuilder: (BuildContext context, int index) => Container(
                child: GestureDetector(
                  onTap: () {print(itemsName[index]);},
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        imgList[index],
                        width: 100,
                        height: 100,
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 10),
                        child: Text(
                          itemsName[index],
                          textAlign: TextAlign.center,
                          style: const TextStyle(
                            fontSize: 13,
                          ),
                        ),
                      ),
                      ElevatedButton(
                        onPressed: () {print(itemsName[index]); },
                        child: Text('Tìm hiểu'),),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

