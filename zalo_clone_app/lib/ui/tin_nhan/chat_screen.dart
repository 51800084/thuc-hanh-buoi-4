import 'package:flutter/material.dart';
import 'package:week4/ui/tin_nhan/inbox_chat.dart';

class ChatScreen extends StatefulWidget {
  static const route = '/';
  const ChatScreen({Key? key}) : super(key: key);

  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  @override
  Widget build(BuildContext context) {
    String title = 'Chat';
    return Scaffold(
      backgroundColor: Colors.grey[300],
      body: Center(
        child: Column(
          children: const [
            BodyListView(),
          ],
        ),
      ),
    );
  }
}

class BodyListView extends StatelessWidget {
  const BodyListView({Key? key}) : super(key: key);

  @override

  Widget build(BuildContext context) {
    List imgList = ['assets/ei.jpg', 'assets/eula.jpg', 'assets/hutao.jpg',];
    List titleList = ['Ei', 'Eula', 'Hutao'];
    List subTitleList = ['đã gửi 1 tin nhắn', 'đã gửi 1 tin nhắn', 'đã gửi 1 tin nhắn'];

    return ListView.builder(
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      itemCount: titleList.length,
      itemBuilder: (context, index) => GestureDetector(
        onTap: () {
          Navigator.of(context).pushNamed(InboxChat.route);
        },
        child: Card(
          margin: EdgeInsets.only(bottom: 1),
          elevation: 0,
          child: ListTile(
            title: Text(titleList[index]),
            subtitle: Text(subTitleList[index]),
            leading: ClipRRect(
                borderRadius: BorderRadius.circular(40),
                child: Image.asset(imgList[index])),
          ),
        ),
      ),
    );
  }
}
