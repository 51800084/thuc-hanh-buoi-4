import 'package:flutter/material.dart';
import 'package:week4/ui/danh_ba/phone_list.dart';
import 'package:week4/ui/kham_pha/discover.dart';
import 'package:week4/ui/nhat_ky/news.dart';
import 'ca_nhan/personal.dart';
import 'tin_nhan/chat_screen.dart';
import 'bottom_navigator.dart';
import 'tin_nhan/inbox_chat.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: {
        '/': (context) => BottomNavigation(),
        '/home': (context) => ChatScreen(),
        '/chat': (context) => InboxChat(),
        '/phone': (context) => PhonesList(),
        '/discover': (context) => DiscoverScreen(),
        '/news': (context) => NewsScreen(),
        '/personal': (context) => PersonalScreen(),
      },
      initialRoute: '/',
    );
  }
}





