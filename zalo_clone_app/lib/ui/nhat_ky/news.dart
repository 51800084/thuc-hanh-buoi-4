import 'package:flutter/material.dart';

class NewsScreen extends StatefulWidget {
  static const route = '/news';
  const NewsScreen({Key? key}) : super(key: key);

  @override
  State<NewsScreen> createState() => _NewsScreenState();
}

class _NewsScreenState extends State<NewsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[300],
      body: SingleChildScrollView(
        child: Column( children: const[
          SearchBar(),
          Moment(),
          NewsArea(),
        ],
        ),
      ),
    );
  }
}

class SearchBar extends StatelessWidget {
  const SearchBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Container(
          color: Colors.white,
          padding: EdgeInsets.all(10),
          child: ListTile(
            title: Text('Hôm nay bạn thế nào?'),
            leading: ClipRRect(
              borderRadius: BorderRadius.circular(30),
              child: Image.asset('assets/hutao.jpg'),
            ),
          ),
        ),
        Container(
          color: Colors.white,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextButton.icon(
                onPressed: null,
                icon: const Icon(Icons.panorama_outlined, size: 20, color: Colors.green,),
                label: Text('Đăng ảnh'),
              ),
              TextButton.icon(
                onPressed: null,
                icon: const Icon(Icons.video_camera_back_outlined, size: 20, color: Colors.pinkAccent,),
                label: Text('Đăng video'),
              ),
              TextButton.icon(
                onPressed: null,
                icon: const Icon(Icons.photo_album_outlined, size: 20, color: Colors.blue,),
                label: Text('Tạo album'),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class Moment extends StatelessWidget {
  const Moment({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 12),
      decoration: BoxDecoration(color: Colors.white),
      padding: EdgeInsets.symmetric(vertical: 12),
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.only(left: 12, bottom: 12, top: 12),
                child: Text(
                    'Khoảng khắc',
                    style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.w500,
                    ),
                ),
              )
            ],
          ),
          Container(
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.only(left: 8),
            height: 100,
            child: FittedBox(
              fit: BoxFit.fill,
              child: Image.asset('assets/hutao_story.png'),
            ),
          )
        ],
      ),
    );
  }
}

class NewsArea extends StatelessWidget {
  const NewsArea({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        shrinkWrap: true,
        itemCount: 4,
        itemBuilder: (context, index) => Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              color: Colors.white,
              child: ListTile(
                leading: ClipRRect(
                  borderRadius: BorderRadius.circular(30),
                  child: Image.asset('assets/hutao.jpg', width: 40,),
                ),
                title: Text(
                  'Hu Tao',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                subtitle: Text(
                    'Thứ 6 ngày 13',
                    style: TextStyle(fontSize: 10),
                ),
                trailing: IconButton(
                  icon: const Icon(Icons.more_horiz_outlined , size: 20, color: Colors.black,),
                  onPressed: () {
                    print('More');
                  },
                ),
              ),
            ),
            Container(
              color: Colors.white,
              width: double.infinity,
              padding: EdgeInsets.all(12),
              margin: EdgeInsets.only(bottom: 8),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('Đây là một câu rất dàiiiiiiiiiiiiiiiiiii'),
                  ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: Image.asset('assets/hutao.jpg'),
                  )
                ],
              ),
            ),
          ],
        ));
  }
}



