import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class FriendsList extends StatelessWidget {
  const FriendsList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: const [
          TopView(),
          BodyView(),
        ],
      ),
    );
  }
}

class TopView extends StatelessWidget {
  const TopView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GestureDetector(
          onTap: () {
            // Navigator.of(context).pushNamed(InboxChat.route);
            {
              print('Lời mời kết bạn');
            }
          },
          child: ListTile(
            title: Text('Lời mời kết bạn'),
            leading: ClipRRect(
              borderRadius: BorderRadius.circular(40),
              child: Icon(
                Icons.group,
                size: 40,
                color: Colors.blue,
              ),
            ),
          ),
        ),
        GestureDetector(
          onTap: () {
            print('Danh bạ máy');
          },
          child: ListTile(
            title: Text('Danh bạ máy '),
            subtitle: Text('Các liên hệ có dùng Zalo'),
            leading: ClipRRect(
              borderRadius: BorderRadius.circular(40),
              child: Icon(
                Icons.phone_rounded,
                size: 40,
                color: Colors.green,
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class BodyView extends StatelessWidget {
  const BodyView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 2,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Container(
              color: Colors.white,
              child: const TabBar(
                  indicatorWeight: 1,
                  unselectedLabelColor: Colors.black38,
                  isScrollable: true,
                  labelColor: Colors.blue,
                  tabs: [
                    Tab(
                      child: Text('Tất cả'),
                    ),
                    Tab(
                      child: Text('Mới truy cập'),
                    )
                  ]),
            ),
            SizedBox(
              height: 1000,
              child: TabBarView(children: [
                Container(
                  child: Text('hello1'),
                ),
                Container(
                  child: Text('hello2'),
                ),
              ]),
            )
          ],
        ));
  }
}


