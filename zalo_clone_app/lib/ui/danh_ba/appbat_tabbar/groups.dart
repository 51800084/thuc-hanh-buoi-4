import 'package:flutter/material.dart';
import 'package:week4/ui/tin_nhan/inbox_chat.dart';

class GroupsList extends StatelessWidget {
  const GroupsList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: const[
          TopView(),
          BodyView(),
          BodyListView(),
        ],
      ),
    );
  }
}
class TopView extends StatelessWidget {
  const TopView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: GestureDetector(
        onTap: () {
          print('Tạo nhóm mới');
        },
        child: ListTile(
          title: Text('Tạo nhóm mới'),
          leading: Icon(Icons.group_add_outlined, size: 40, color: Colors.lightBlue,),
        ),
      ),);
  }
}

class BodyView extends StatelessWidget {
  const BodyView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Text('Tạo nhóm với:'),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextButton.icon(
                  onPressed: null,
                  icon: const Icon(Icons.calendar_month_outlined, size: 30, color: Colors.blue,),
                  label: Text('Lịch'),
              ),
              TextButton.icon(
                  onPressed: null,
                  icon: const Icon(Icons.access_alarms_outlined, size: 30,color: Colors.pinkAccent,),
                  label: Text('Nhắc hẹn'),
              ),
              TextButton.icon(
                  onPressed: null,
                  icon: const Icon(Icons.change_circle_outlined, size: 30,color: Colors.purple,),
                  label: Text('Nhóm offline'),
              ),
            ],
          ),
        ],
    );
  }
}

class BodyListView extends StatelessWidget {
  const BodyListView({Key? key}) : super(key: key);

  @override

  Widget build(BuildContext context) {
    List imgList = ['assets/ei.jpg', 'assets/eula.jpg', 'assets/hutao.jpg',];
    List titleList = ['Ei', 'Eula', 'Hutao'];
    List subTitleList = ['đã gửi 1 tin nhắn', 'đã gửi 1 tin nhắn', 'đã gửi 1 tin nhắn'];

    return ListView.builder(
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      itemCount: titleList.length,
      itemBuilder: (context, index) => GestureDetector(
        onTap: () {
          Navigator.of(context).pushNamed(InboxChat.route);
        },
        child: Card(
          margin: EdgeInsets.only(bottom: 1),
          elevation: 0,
          child: ListTile(
            title: Text(titleList[index]),
            subtitle: Text(subTitleList[index]),
            leading: ClipRRect(
                borderRadius: BorderRadius.circular(40),
                child: Image.asset(imgList[index])),
          ),
        ),
      ),
    );
  }
}

