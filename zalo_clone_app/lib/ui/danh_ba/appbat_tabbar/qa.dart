import 'package:flutter/material.dart';

class QAList extends StatelessWidget {
  const QAList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: const[
          TopView(),
          BodyView(),
        ],
      ),
    );
  }
}

class TopView extends StatelessWidget {
  const TopView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: GestureDetector(
        onTap: () {
          print('Tìm thêm Offical Account');
        },
        child: ListTile(
          title: Text('Tìm thêm Offical Account'),
          leading: Icon(Icons.perm_media_outlined, size: 40, color: Colors.purple,),
        ),
      ),);
  }
}

class BodyView extends StatelessWidget {
  const BodyView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Container(
          margin: const EdgeInsets.all(10.0),
          child:Text('Offical Account đang quan tâm'),),
        Container(
          margin: const EdgeInsets.all(10.0),
          child: GestureDetector(
            onTap: () {
              print('Báo mới');
            },
            child: ListTile(
              title: Text('Báo mới'),
              leading: ClipRRect(
                borderRadius: BorderRadius.circular(40),
                child: Image.asset('assets/baomoi.png'),
              ),
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.all(10.0),
          child: GestureDetector(
            onTap: () {
              print('Zalopay');
            },
            child: ListTile(
              title: Text('ZaloPay'),
              leading: ClipRRect(
                borderRadius: BorderRadius.circular(40),
                child: Image.asset('assets/zalopay.png'),
              ),
            ),
          ),
        )
      ],
    );
  }
}

