import 'package:flutter/material.dart';
import 'package:week4/ui/danh_ba/appbat_tabbar/friends.dart';
import 'package:week4/ui/danh_ba/appbat_tabbar/groups.dart';
import 'package:week4/ui/danh_ba/appbat_tabbar/qa.dart';
import '../danh_ba/appbat_tabbar/friends.dart';


class PhonesList extends StatefulWidget {
  static const route = '/phone';
  const PhonesList({Key? key}) : super(key: key);

  @override
  _PhonesListState createState() => _PhonesListState();
}

class _PhonesListState extends State<PhonesList> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container (
        child: Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: <Widget>[
         DefaultTabController(
             length: 3,
             initialIndex: 0,
             child: Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: <Widget>[
               Container(
                 child: TabBar(
                   labelColor: Colors.black,
                   unselectedLabelColor: Colors.grey,
                   tabs: [
                     Tab(text: 'BẠN BÈ'),
                     Tab(text: 'NHÓM'),
                     Tab(text: 'QA'),
                   ],
                 ),
               ),
               Container(
                 height: 400,
                 decoration: BoxDecoration(
                   border: Border(top: BorderSide(color: Colors.grey, width: 0.5))
                 ),
                 child: TabBarView(children: <Widget>[
                   FriendsList(),
                   GroupsList(),
                   QAList(),
                 ],),
               )
             ],))
        ],),
      ),
    );
  }
}
