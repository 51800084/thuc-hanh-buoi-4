import 'package:flutter/material.dart';

class PersonalScreen extends StatefulWidget {
  static const route = '/personal';
  const PersonalScreen({Key? key}) : super(key: key);

  @override
  State<PersonalScreen> createState() => _PersonalScreenState();
}

class _PersonalScreenState extends State<PersonalScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[300],
      body: SingleChildScrollView(
        child: Column(
          children: const[
            User(),
            FunctionList(),
          ],
        ),
      ),
    );
  }
}

class User extends StatelessWidget {
  const User({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        color: Colors.white,
      ),
      margin: EdgeInsets.only(bottom: 12),
      height: 80,
      child: ListTile(
        title: Text('FI'),
        subtitle: GestureDetector(
          onTap: () {
            print('Xem trang cá nhân');
          },
            child: Text('Xem trang cá nhân')),
        leading: Container(
          alignment: Alignment.center,
          width: 50,
          height: 50,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(30),
            child: Image.asset('assets/hutao.jpg'),
          ),
        ),
        trailing: Icon(Icons.switch_account_outlined, color: Colors.blue,),
      ),
    );
  }
}

class FunctionList extends StatelessWidget {
  const FunctionList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    List functionListViewTitle= ['Ví QR', 'Cloud của tôi', 'Tài khoản và bảo mật', 'Quyền riêng tư',];
    List functionListViewLeadingIcons= [
      Icons.wallet_membership_outlined,
      Icons.wb_cloudy_outlined,
      Icons.shield_outlined,
      Icons.lock_outline,
    ];

    return ListView.builder(
        shrinkWrap: true,
        itemCount: 4,
        itemBuilder: (context, index) => GestureDetector(
          onTap: () {
            print(functionListViewTitle[index]);
          },
          child: Card(
            margin: EdgeInsets.only(bottom: 4),
            elevation: 0,
            child: ListTile(
              tileColor: Colors.white,
              title: Text(functionListViewTitle[index]),
              leading: Icon(functionListViewLeadingIcons[index], color: Colors.lightBlueAccent,),
            ),
          ),
        )
    );
  }
}

